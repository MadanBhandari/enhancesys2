$(document).ready(function(){
  // set full screen
  window.onload = maxWindow;
  function maxWindow() {
    window.moveTo(0, 0);
    if (document.all) {
        top.window.resizeTo(screen.availWidth, screen.availHeight);
    }
    else if (document.layers || document.getElementById) {
      if (top.window.outerHeight < screen.availHeight || top.window.outerWidth < screen.availWidth) {
          top.window.outerHeight = screen.availHeight;
          top.window.outerWidth = screen.availWidth;
      }
    }
  }
  // search-area fixed when scroll
  $(window).scroll(function() {
    if( $(this).scrollTop() > 100 ) {
      $('.search-area').addClass('fix-search');
    } else {
      $('.search-area').removeClass('fix-search');
    }
  });
  // set height of next-page link
  var theight = $('.next-page').prev().height();
  $('.next-page').css('height', theight);
  // navigation menu
  $('#nav-icon').click(function() {
    setTimeout(function() {
      $("#menu" ).toggleClass("open-menu");
      $("#page" ).toggleClass("opened-menu");
      $("#menublock").toggleClass("full-block");
      $("#nav-icon > i" ).toggleClass("cross");
    }, 400);
  });
  // close notification on back icon
  $('#notification-back').click(function() {
    $("#notification").removeClass("open-notification");
    $("#page").removeClass("opened-notification");
    $("#menublock").removeClass("full-block");
  });
  // open menu on swiperight
  Hammer(page).on("swiperight", function() {
    if ($("#notification").hasClass("open-notification")) {
      $("#notification").removeClass("open-notification");
      $("#page").removeClass("opened-notification");
      $("#menublock").removeClass("full-block");
    //  console.log("open menu by swiperight");
    }
    else {
      $("#menu").addClass("open-menu");
      $("#page").addClass("opened-menu");
      $("#menublock").addClass("full-block");
      $("#nav-icon > i" ).toggleClass("cross");
    //  console.log("open menu by swiperight");
    }
  });
  // open notification on swipeleft
  Hammer(page).on("swipeleft", function() {
    if ($("#menu").hasClass("open-menu")) {
      $("#menu").removeClass("open-menu");
      $("#page").removeClass("opened-menu");
      $("#menublock").removeClass("full-block");
      $("#nav-icon > i" ).toggleClass("cross");
    //  console.log("open menu by swiperight");
    }
    else {
      $("#notification").addClass("open-notification");
      $("#page").addClass("opened-notification");
      $("#menublock").addClass("full-block");
    //  console.log("open menu by swiperight");
    }
  });
  // close menu on swipeleft
  Hammer(menu).on("swipeleft", function() {
    $("#menu").removeClass("open-menu");
    $("#page").removeClass("opened-menu");
    $("#menublock").removeClass("full-block");
    $("#nav-icon > i" ).toggleClass("cross");
  //  console.log("close menu by swipeleft");
  });
  // close notification on swiperight
  Hammer(notification).on("swiperight", function() {
    $("#notification").removeClass("open-notification");
    $("#page").removeClass("opened-notification");
    $("#menublock").removeClass("full-block");
  //  console.log("close notification by swiperight");
  });
  // close notification & menu on click menublock
  $("#menublock").click(function() {
    if ($("#menu").hasClass("open-menu")) {
      $("#menu").removeClass("open-menu");
      $("#page").removeClass("opened-menu");
      $("#menublock").removeClass("full-block");
      $("#nav-icon > i" ).toggleClass("cross");
    }
    $("#notification").removeClass("open-notification");
    $("#page").removeClass("opened-menu opened-notification");
    $("#menublock").removeClass("full-block");
  });

  // open notification on click notification-icon
  $('#notification-icon').click(function() {
    setTimeout(function() {
      $("#notification" ).toggleClass("open-notification");
      $("#page" ).toggleClass("opened-notification");
      $("#menublock").toggleClass("full-block");
    }, 400);
  });
  //Change Icon on Accordion //
  $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".arrow-down").addClass("arrow-up").removeClass("arrow-down");
    }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".arrow-up").addClass("arrow-down").removeClass("arrow-up");
  });
  //Ripple Effecet onclick //
  var parent, ink, d, x, y;
  $(".rpl").click(function(e){
    parent = $(this).parent();
    //create .ink element if it doesn't exist
    if(parent.find(".ink").length == 0)
      parent.prepend("<span class='ink'></span>");

    ink = parent.find(".ink");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animate");

    //set size of .ink
    if(!ink.height() && !ink.width())
    {
      //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
      d = Math.max(parent.outerWidth(), parent.outerHeight());
      ink.css({height: d, width: d});
    }

    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width()/2;
    y = e.pageY - parent.offset().top - ink.height()/2;

    //set the position and add class .animate
    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
  })

  // navigate URL
  $('.navigate-url').click(function() {
    window.location.href=$(this).data('url');
  })

  //language page
  $('.language-row').click(function() {
    $('.language-row').each(function() {
      $(this).removeClass('selected');
    });
    $(this).addClass('selected');
  });

  // add items on cart
  $('.notification-no').hide();
  $('.btn-number').click(function(e){
    function pad (str, max) {
      str = str.toString();
      return str.length < max ? pad("0" + str, max) : str;
    }
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $(this).closest('div').find("input[name='"+fieldName+"']");
    $(input).attr('data-cart', 0);
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
      if(type == 'minus') {
        if(currentVal > input.attr('min')) {
            input.val(pad(currentVal - 1,4)).change();
        }
        if(parseInt(input.val()) == input.attr('min')) {
            $(this).attr('disabled', true);
        }
      }else if(type == 'plus') {
        if(currentVal < input.attr('max')) {
          input.val(pad(currentVal + 1,4)).change();
        }
        if(parseInt(input.val()) == input.attr('max')) {
          $(this).attr('disabled', true);
        }
      }
    } else {
      input.val(0);
    }
    //add to cart
      if(input.val()>0){
        $(input).attr('data-cart', 1);
      }
      else {
        $(input).attr('data-cart', 0);
      }
      var numItems = $("*[data-cart='1']").length;
      //console.log(numItems);
      if(numItems > 0){
        $('.notification-no').fadeIn().text(numItems);
      }
      else {
        $('.notification-no').fadeOut();
      }
  });
  $('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
  });
  $('.input-number').change(function() {
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
      $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
      $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
      $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
      $(this).val($(this).data('oldValue'));
    }
  });


    // add items to cart/box old
//    if (parseInt($('.input-number').val()) > 0){
//      $('.notification-no').fadeIn().text(parseInt($('.input-number').val()));
//    }
//    else {
//      $('.notification-no').hide();
//    }
//    $('.btn-number').click(function(){
//      if (parseInt($('.input-number').val()) > 0){
//        $('.notification-no').fadeIn().text(parseInt($('.input-number').val()));
//      }
//      else {
//        $('.notification-no').hide();
//      }
//    });

  $(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
  });

  //remove item from cart
  var itemid;
  $('.delete-item').click(function(){
   $('#remove-item').modal('show');
    var item = $(this).parent().closest('.item');
    itemid = $(item).data('item-id');
  });
  $('#removeItem').click(function(){
    $('#remove-item').modal('hide');
    $('.item[data-item-id="' + itemid + '"]').fadeOut();
  });
  var imgid;
  $('.delete-img').click(function(){
   $('#remove-item').modal('show');
    var img = $(this).parent().closest('figure');
    imgid = $(img).data('item-id');
  });
  $('#removeItem').click(function(){
    $('#remove-item').modal('hide');
    $('figure[data-item-id="' + imgid + '"]').fadeOut();
  });
 // show ripple effect and proceed the link
  $('.rpl[href$=".html"]').click(function (e) {
    if(this.getAttribute("href").length)
      {
        e.preventDefault();
        var goTo = this.getAttribute("href"); // store a href
        setTimeout(function(){
          window.location = goTo;
        },400);
      }
  });
});
